package main

import (
	"context"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	masonGo "gitlab.com/MasonAmericaPublic/go-sdk.git"
	"gitlab.com/MasonAmericaPublic/go-sdk.git/deploy"
	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

func createDeployCommand(mason *masonGo.Mason) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "deploy",
		Short: "deploy existing artifacts to groups on the Mason Platform",
		Long:  "This command will deploy artifacts to a specific group. See subcommands for details on deploying each artifact type.",
	}

	cmd.AddCommand(
		deployOtaCommand(mason),
		deployAPKCommand(mason),
		deployConfigCommand(mason))
	return cmd
}

func deployAPKCommand(mason *masonGo.Mason) *cobra.Command {
	var version, group, name string
	var push bool

	var cmd = &cobra.Command{
		Use:   "apk",
		Short: "deploy an APK to a group",
		Example: "mason deploy apk --name com.company.sampleApp --version 3 --group sampleGroup -p\n" +
			"mason deploy apk --name com.company.sampleApp --version latest --group sampleGroup -p",
		Long: "This command will deploy a registered APK to a specific group.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Info(fmt.Sprintf("deploying APK: name: %s, version: %s, group: %s", name, version, group))

			deploymentGroup, err := mason.GetDeploymentGroupByName(context.Background(), group)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}

			deployment, err := mason.Deploy(context.Background(), deploy.DeployParameters{
				Name:    name,
				GroupId: deploymentGroup.ID,
				Version: version,
				Type:    deploy.DeploymentTypeAPK,
				Push:    push,
			})
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			log.Info("apk deployed", prettyPrint(deployment))
		},
	}
	setStandardDeploymentFlags(cmd, &version, &group, &push, "APK")
	cmd.Flags().StringVarP(&name, "name", "n", "", "Package name of APK to deploy")
	_ = cmd.MarkFlagRequired("name")
	return cmd
}

func deployConfigCommand(mason *masonGo.Mason) *cobra.Command {
	var version, group, name string
	var push bool

	var cmd = &cobra.Command{
		Use:   "config",
		Short: "deploy a config to a group",
		Example: "mason deploy config --name sampleProject --version 1 --group sampleGroup -p\n" +
			"mason deploy config --name sampleProject --version latest --group sampleGroup -p",
		Long: "This command will deploy a registered config to a specific group.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Info(fmt.Sprintf("deploying config: name: %s, version: %s, group: %s", name, version, group))

			deploymentGroup, err := mason.GetDeploymentGroupByName(context.Background(), group)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}

			deployment, err := mason.Deploy(context.Background(), deploy.DeployParameters{
				Name:    name,
				GroupId: deploymentGroup.ID,
				Version: version,
				Type:    deploy.DeploymentTypeConfig,
				Push:    push,
			})
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			log.Info("config deployed", prettyPrint(deployment))
		},
	}
	setStandardDeploymentFlags(cmd, &version, &group, &push, "config")
	cmd.Flags().StringVarP(&name, "name", "n", "", "Name of the project the config belongs to")
	_ = cmd.MarkFlagRequired("name")

	return cmd
}

func deployOtaCommand(mason *masonGo.Mason) *cobra.Command {
	var version, group string
	var push bool

	var cmd = &cobra.Command{
		Use:     "ota",
		Short:   "deploy an OTA to a group",
		Example: "mason deploy ota --version 2.13.0 --group sampleGroup -p",
		Long:    "This command will deploy a mason OS OTA to a specific group.",
		Run: func(cmd *cobra.Command, args []string) {
			log.Info(fmt.Sprintf("deploying OTA: version: %s, group: %s", version, group))

			deploymentGroup, err := mason.GetDeploymentGroupByName(context.Background(), group)
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}

			deployment, err := mason.Deploy(context.Background(), deploy.DeployParameters{
				Name:    "mason-os",
				GroupId: deploymentGroup.ID,
				Version: version,
				Type:    deploy.DeploymentTypeOTA,
				Push:    push,
			})
			if err != nil {
				log.Error(err.Error())
				os.Exit(1)
			}
			log.Info("OTA deployed", prettyPrint(deployment))
		},
	}
	setStandardDeploymentFlags(cmd, &version, &group, &push, "OTA")
	return cmd
}

func setStandardDeploymentFlags(cmd *cobra.Command, version, group *string, push *bool, artifactType string) {
	cmd.Flags().StringVarP(version, "version", "v", "", fmt.Sprintf("%s version to deploy", cases.Title(language.English).String(artifactType)))
	cmd.Flags().StringVarP(group, "group", "g", "", fmt.Sprintf("Name of the group to deploy the %s to", artifactType))
	cmd.Flags().BoolVarP(push, "push", "p", false, fmt.Sprintf("Push %s immediately to devices", artifactType))
	_ = cmd.MarkFlagRequired("version")
	_ = cmd.MarkFlagRequired("group")
}
