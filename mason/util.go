package main

import (
	"encoding/json"
	"fmt"
)

// WindowsFilenameSafeTimestampFormat is a timestamp format that is used for
// creating files in windows that have timestamps in them.
// is needed due to windows file system not accepting several special characters
// such as `:` which is vital when making a file with a timestamp in it. This timestamp here is a simple format
// that removes the special characters not accepted by windows.
const WindowsFilenameSafeTimestampFormat = "2006-01-02--15-04-05MST"

func DumpJSON(i interface{}) {
	s, _ := json.MarshalIndent(i, "", "  ")
	fmt.Println(string(s))
}
