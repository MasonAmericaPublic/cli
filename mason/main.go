package main

import (
	"fmt"
	"os"
	"runtime"

	"github.com/spf13/cobra"
	masonGo "gitlab.com/MasonAmericaPublic/go-sdk.git"
)

const cliVersion = "2.1.11"
const cliUserAgent = "Mason-cli-v2"

func main() {
	var apiKey string
	mason := &masonGo.Mason{}

	var rootCmd = &cobra.Command{
		Use:     "mason",
		Version: cliVersion,
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			err := masonGo.NewMason(mason, apiKey)
			runtimeArch := fmt.Sprintf("runtime %s/%s", runtime.GOOS, runtime.GOARCH)
			mason.AddUserAgent(cliUserAgent, cliVersion, &runtimeArch)
			if err != nil {
				fmt.Println(err.Error())
				os.Exit(1)
			}
		},
	}

	rootCmd.CompletionOptions = cobra.CompletionOptions{
		DisableDefaultCmd: true,
		DisableNoDescFlag: false,
	}

	rootCmd.SetVersionTemplate(getVersionString())
	rootCmd.PersistentFlags().StringVarP(&apiKey, "api-key", "", "", "Mason Platform API key")

	configureLogger()

	rootCmd.AddCommand(
		createDeployCommand(mason),
		createRegisterCommand(mason),
		createXrayCommand(mason),
		createCreateCommand(mason),
		createGetCommand(mason))

	err := rootCmd.Execute()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func createRegisterCommand(mason *masonGo.Mason) *cobra.Command {
	var registerCmd = &cobra.Command{
		Use:   "register",
		Short: "register artifacts to the Mason Platform",
		Long:  "This command will register artifacts with the Mason Platform. See subcommands for details on how to register on artifact type.",
	}
	registerCmd.AddCommand(
		registerAPKsCmd(mason),
		registerBootanimationCmd(mason),
		registerSplashCmd(mason),
		registerConfigCmd(mason))
	return registerCmd
}

func createCreateCommand(mason *masonGo.Mason) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "create",
		Short: "Create commands for create action in Mason Platform",
		Long: `This command will create resources in Mason Platform.
			See subcommands for details on each resource create action.`,
	}
	cmd.AddCommand(
		createProjectCmd(mason))
	return cmd
}

func createGetCommand(mason *masonGo.Mason) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "get",
		Short: "Get commands for get action in Mason Platform",
		Long: `This command will retrieve resources in Mason Platform.
			See subcommands for details on each resource get action.`,
	}
	cmd.AddCommand(
		getProjectCmd(mason))
	return cmd
}

func createXrayCommand(mason *masonGo.Mason) *cobra.Command {
	var xrayCmd = &cobra.Command{
		Use:   "xray",
		Short: "X-Ray commands for interacting with devices",
		Long:  "This command will interact with devices via X-Ray. See subcommands for details on each X-Ray action.",
	}

	var deviceId string
	xrayCmd.PersistentFlags().StringVarP(&deviceId, "deviceId", "d", "", "device UUID")
	_ = xrayCmd.MarkPersistentFlagRequired("deviceId")

	xrayCmd.AddCommand(
		adbProxyCmd(mason, &deviceId),
		logcatCmd(mason, &deviceId),
		listFilesCmd(mason, &deviceId),
		getFileCmd(mason, &deviceId),
		putFileCmd(mason, &deviceId),
		deleteFileCmd(mason, &deviceId),
		describeLogBuffersCmd(mason, &deviceId),
		getLogsCmd(mason, &deviceId),
		installApkCmd(mason, &deviceId),
		uninstallApkCmd(mason, &deviceId),
		getBugReportCmd(mason, &deviceId),
		getScreenCaptureCmd(mason, &deviceId),
	)
	return xrayCmd
}

func getVersionString() string {
	return fmt.Sprintf("Mason CLI %s"+
		"\nCopyright (C) 2019 Mason America (https://bymason.com)"+
		"\nLicense Apache 2.0 <https://www.apache.org/licenses/LICENSE-2.0>\n", cliVersion)
}
